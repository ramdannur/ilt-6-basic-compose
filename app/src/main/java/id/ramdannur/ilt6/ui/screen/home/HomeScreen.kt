package id.ramdannur.ilt6.ui.screen.home

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import id.ramdannur.ilt6.CustomItem

@Composable
fun HomeScreen(modifier : Modifier = Modifier) {
    LazyColumn(modifier = modifier) {
        items(10){
            CustomItem()
        }
    }
}