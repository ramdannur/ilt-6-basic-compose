package id.ramdannur.ilt6

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import id.ramdannur.ilt6.ui.screen.favorite.FavoriteScreen
import id.ramdannur.ilt6.ui.screen.home.HomeScreen
import id.ramdannur.ilt6.ui.screen.profile.ProfileScreen
import id.ramdannur.ilt6.ui.theme.ILT6Theme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ILT6Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MyApp()
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MyApp(
    navController: NavHostController = rememberNavController(),
    modifier: Modifier = Modifier
) {
    Scaffold(
        bottomBar = {
            BottomBar(navController = navController)
        },
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "My App")
                },
            )
        }) { contentPadding ->
        NavHost(
            navController = navController,
            startDestination = "home",
            modifier = Modifier.padding(contentPadding)
        ) {
            composable("home") {
                HomeScreen()
            }
            composable("favorite") {
                FavoriteScreen()
            }
            composable("profile") {
                ProfileScreen()
            }
        }
    }
}

@Composable
fun CustomItem(modifier: Modifier = Modifier) {
    Row(modifier = modifier) {
        Icon(
            imageVector = Icons.Default.AccountCircle,
            contentDescription = null,
            modifier = Modifier
                .size(80.dp)
                .padding(12.dp)
        )
        Column(modifier = modifier) {
            Text("John Doe")
            Text("Lorem ipsum dolort sit amet")
        }
    }
}

@Composable
fun BottomBar(navController: NavHostController, modifier: Modifier = Modifier) {
    NavigationBar(modifier = modifier) {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry?.destination?.route

        val listRoute = listOf(
            "home",
            "favorite",
            "profile"
        )

        val listTitle = listOf(
            "Home",
            "Favorite",
            "Profile"
        )

        val listImage = listOf(
            Icons.Default.Home,
            Icons.Default.Favorite,
            Icons.Default.AccountCircle
        )

        listTitle.mapIndexed { index, title ->
            NavigationBarItem(
                label = {
                    Text(text = title)
                },
                icon = {
                    Icon(
                        imageVector = listImage[index], contentDescription = title
                    )
                },
                selected = currentRoute == listRoute[index], onClick = {
                    navController.navigate(listRoute[index]) {
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState
                        }

                        restoreState = true
                        launchSingleTop = true
                    }
                }
            )
        }
    }
}

@Preview("Light Mode", showBackground = true)
@Preview("Dark Mode", uiMode = UI_MODE_NIGHT_YES, showBackground = true)
@Composable
fun GreetingPreview() {
    ILT6Theme {
        MyApp()
    }
}